# Keytest
![License badge](https://raster.shields.io/badge/license-The%20Unlicense-lightgrey.png "License badge")
![Version badge](https://raster.shields.io/badge/version-0.2-lightgrey.png "Version badge")


![Alt text](thumbnail.png?raw=true "Thumbnail")

Keycode test of your device using pure JavaScript.

## Demo

See in action on [GitLab pages](https://ethicist.gitlab.io/keytest) or [here](https://keytest.surge.sh/)

## Contributing

Please feel free to submit pull requests.
Bugfixes and simple non-breaking improvements will be accepted without any questions.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the [LICENSE](LICENSE) file or [unlicense.org](https://unlicense.org).
